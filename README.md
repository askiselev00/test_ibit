## Poetry

This project uses poetry. It's a modern dependency management
tool.

To run the project use this set of commands:

```bash
poetry install
poetry run python -m ibit_test
```

This will start the server on the configured host.

You can read more about poetry here: https://python-poetry.org/

## Websocket connection

```bash
ws://127.0.0.1:8080/api/ws
```

## Configuration

This application can be configured with environment variables.

You can create `.env` file in the root directory and place all
environment variables here.

All environment variabels should start with "IBIT_TEST_" prefix.

For example if you see in your "ibit_test/settings.py" a variable named like
`random_parameter`, you should provide the "IBIT_TEST_RANDOM_PARAMETER"
variable to configure the value. This behaviour can be changed by overriding `env_prefix` property
in `ibit_test.settings.Settings.Config`.

An exmaple of .env file:
```bash
IBIT_TEST_RELOAD="True"
IBIT_TEST_PORT="8000"
IBIT_TEST_ENVIRONMENT="dev"
```

You can read more about BaseSettings class here: https://pydantic-docs.helpmanual.io/usage/settings/

## Running tests

1. Run the pytest.
```bash
pytest -vv .
```
