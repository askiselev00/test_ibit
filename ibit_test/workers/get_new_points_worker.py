import asyncio
import json
import threading

import httpx

from ibit_test.db.models.tables import Asset, Point


async def new_point_worker(event: threading.Event) -> None:  # noqa: WPS210
    """
    Gets new points.

    Makes request and parce text into json, before remove
    unnecessary characters left and right.

    Saves new point for each asset.

    :param event: Event attached to the worker thread.
    """
    asset_symbols = await Asset.select(Asset.symbol).output(
        as_list=True,
    )
    async with httpx.AsyncClient() as client:
        while event.is_set():
            to_save_assets = []
            response = await client.get(
                "https://ratesjson.fxcm.com/DataDisplayer",
                timeout=1,
            )
            resp_data = response.text.lstrip("null(")  # noqa: B005
            resp_data = resp_data.rstrip(");\n")
            r_json = json.loads(resp_data)
            for asset in r_json["Rates"]:
                if asset["Symbol"] in asset_symbols:
                    bid, ask = float(asset["Bid"]), float(asset["Ask"])  # noqa: WPS221
                    value = (bid + ask) / 2

                    asset = (
                        await Asset.select(
                            Asset.id,
                        )
                        .where(
                            Asset.symbol == asset["Symbol"],
                        )
                        .first()
                    )

                    to_save_assets.append(
                        {
                            "asset": asset["id"],
                            "value": value,
                        },
                    )

            for to_save_asset in to_save_assets:
                await Point(
                    **to_save_asset,
                ).save()
            await asyncio.sleep(1)
