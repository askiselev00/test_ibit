import asyncio
import datetime
import threading
from typing import Any, Dict

from ibit_test.db.models.tables import Asset, Point
from ibit_test.web.api.websockets.manager import ws_manager
from ibit_test.web.api.websockets.schema import (
    AssetsSchema,
    IbitResponse,
    LastHalfAnHourAssetsSchema,
)


async def send_subscription_data(
    client_addr: str,
    asset_id: int,
    run_event: threading.Event,
) -> None:
    """
    Sends new point to websocket.

    :param client_addr: unique connection's id..
    :param asset_id: unique asset id.
    :param run_event: Event attached to Thread.
    """
    while run_event.is_set():
        new_point = (
            await Point.select()
            .where(
                Point.asset == asset_id,
            )
            .order_by(
                Point.time,
                ascending=False,
            )
            .first()
        )
        await ws_manager.send_message(
            response=IbitResponse(
                action="point",
                message=new_point,
            ),
            client_addr=client_addr,
        )
        await asyncio.sleep(1)


async def get_assets(  # noqa: WPS234
    **kwargs: Any,
) -> IbitResponse:
    """
    Returns all assets from the database.

    :param kwargs: named arguments.

    :returns: dict.
    """
    return IbitResponse(
        action="assets",
        message=AssetsSchema(assets=await Asset.select()).dict(),
    )


async def subscribe(
    client_addr: str,
    request_data: Dict[str, int],
) -> None:
    """
    Subscribes the user to the latest assets updates.

    :param client_addr: unique connection's id.
    :param request_data: data with assetId info.
    """
    client = ws_manager.clients[client_addr]
    if client.event and client.subs_worker:
        client.event.clear()
        client.subs_worker.join()

    last30minutes_asset_values = await Point.select(
        Point.asset,
        Point.time,
        Point.value,
    ).where(
        Point.asset == request_data["assetId"],
        Point.time <= datetime.datetime.now(),
    )

    await ws_manager.send_message(
        response=IbitResponse(
            action="asset_history",
            message=LastHalfAnHourAssetsSchema(
                points=last30minutes_asset_values,
            ).dict(),
        ),
        client_addr=client_addr,
    )

    run_event = threading.Event()
    run_event.set()
    subs_worker = threading.Thread(
        target=asyncio.run,
        args=(send_subscription_data(client_addr, request_data["assetId"], run_event),),
    )
    client.event = run_event
    client.subs_worker = subs_worker
    subs_worker.start()
