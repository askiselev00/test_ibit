import logging
import threading
from dataclasses import dataclass
from typing import Dict, Optional

from fastapi import WebSocket
from fastapi.encoders import jsonable_encoder

from ibit_test.web.api.websockets.schema import IbitResponse

logger = logging.getLogger(__name__)


@dataclass
class Connection:
    """Class for connection."""

    socket: WebSocket
    event: Optional[threading.Event] = None
    subs_worker: Optional[threading.Thread] = None


class ConnectionsManager:
    """Class for managing websocket connections."""

    clients: Dict[str, Connection] = {}

    def add_client(
        self,
        websocket: WebSocket,
        connection_id: str,
    ) -> None:
        """
        Remembers client in memory.

        :param connection_id: unique connection's identifier.
        :param websocket: current websocket connection.
        """
        self.clients[connection_id] = Connection(
            socket=websocket,
        )

    def remove_client(self, connection_id: str) -> None:
        """
        Removes client connection from memory.

        :param connection_id: unique connection's identifier.
        """
        client = self.clients[connection_id]
        if client.event and client.subs_worker:
            client.event.clear()
            client.subs_worker.join()
        del self.clients[connection_id]

    async def send_message(
        self,
        response: IbitResponse,
        client_addr: str,
    ) -> None:
        """
        Sends message to user.

        :param response: Message for user.
        :param client_addr: unique connection's id.
        """
        if connection := self.clients[client_addr]:
            try:
                await connection.socket.send_json(
                    jsonable_encoder(response.dict(exclude_none=True)),
                )
            except Exception as exc:
                logger.info(f"Can't send message to {client_addr}. Cause: {exc}")


ws_manager = ConnectionsManager()
