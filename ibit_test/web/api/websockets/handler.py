import json

from fastapi import WebSocket

from ibit_test.web.api.websockets.actions import get_assets, subscribe
from ibit_test.web.api.websockets.manager import ws_manager
from ibit_test.web.api.websockets.schema import IbitRequest, IbitResponse

available_action = {
    "assets": get_assets,
    "subscribe": subscribe,
}


async def handle_request(  # noqa: C901
    message_text: str,
    client_addr: str,
    websocket: WebSocket,
) -> None:
    """
    Process incoming request.

    This function parses message from websocket and tries to process the
    request.

    :param message_text: text of incoming message.
    :param client_addr: unique connection's id.
    :param websocket: actual websocket to write responses.
    """
    try:
        message_json = json.loads(message_text)
    except ValueError:
        error = IbitResponse(
            message={
                "error": "Unknown format",
            },
        )
        await ws_manager.send_message(
            response=error,
            client_addr=client_addr,
        )
        return

    try:
        parsed_request = IbitRequest.parse_obj(message_json)
    except ValueError:
        error = IbitResponse(
            message={
                "error": "Wrong message format",
            },
        )
        await ws_manager.send_message(
            response=error,
            client_addr=client_addr,
        )
        return

    if resolved_method := available_action.get(parsed_request.action):
        response = await resolved_method(  # type: ignore
            client_addr=client_addr,
            request_data=parsed_request.message,
        )

        if response:
            await ws_manager.send_message(
                response=response,
                client_addr=client_addr,
            )
        return

    error = IbitResponse(
        message={"error": "method not found."},
    )
    await websocket.send_json(error.dict(exclude_none=True))
