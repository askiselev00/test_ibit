import asyncio
import os
import threading

import uvicorn

from ibit_test.settings import settings
from ibit_test.workers.get_new_points_worker import new_point_worker


def main() -> None:
    """Entrypoint of the application."""
    os.environ["PICCOLO_CONF"] = "ibit_test.piccolo_conf"
    uvicorn.run(
        "ibit_test.web.application:get_app",
        workers=settings.workers_count,
        host=settings.host,
        port=settings.port,
        reload=settings.reload,
        log_level=settings.log_level.value.lower(),
        factory=True,
    )


if __name__ == "__main__":
    run_event = threading.Event()
    run_event.set()
    worker = threading.Thread(
        target=asyncio.run,
        args=(new_point_worker(run_event),),
    )
    worker.start()
    main()
    run_event.clear()
    worker.join()
