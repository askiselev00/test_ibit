from piccolo import columns
from piccolo.table import Table


class Asset(Table):
    """Table for assets."""

    id = columns.SmallInt(
        primary_key=True,
    )
    symbol = columns.Varchar(
        length=6,
    )


class Point(Table):
    """Data for assets."""

    asset = columns.ForeignKey(
        references=Asset,
    )
    time = columns.Timestamp()
    value = columns.Real()
